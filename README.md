# Progressive Enhancement Simulator

A tool to teach clients about the options for supporting users without Javascript support.

## Possible Improvements

After using the simulator with a client we came up with the following possible improvements:
* Make it clear that the buttons under 'Simulate user' and 'Simulate website' are offering discrete options, perhaps by using a [dropdown button](https://getbootstrap.com/docs/4.0/components/dropdowns/#single-button-dropdowns) or [button group](https://getbootstrap.com/docs/4.3/components/button-group/).
* Prevent the page from scrolling back up to the explanation on every 'non-js' request. It may be good enough (and a good idea in general) to reduce the size of the explanation.
